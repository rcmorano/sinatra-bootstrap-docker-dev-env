#\ -s puma

environment = ENV['RACK_ENV']

require 'sinatra/base'
require './app/controllers/app_controller'

Dir.glob('./app/{models,helpers,controllers}/*.rb').each { |file| require file }

map('/') { run IndexController }
map('/login') { run LoginController }
