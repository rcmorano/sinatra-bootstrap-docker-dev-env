FROM rcmorano/trusty-rvm
RUN apt-get update
RUN apt-get install --assume-yes --force-yes vim git
RUN /bin/bash --login -c 'rvm gemset create sinatra-bootstrap-docker-dev-env'
RUN /bin/bash --login -c 'rvm gemset use sinatra-bootstrap-docker-dev-env; gem install --no-ri --no-rdoc bundle sinatra sinatra-contrib padrino yard-sinatra puma'

CMD ["/bin/bash","--login"]
