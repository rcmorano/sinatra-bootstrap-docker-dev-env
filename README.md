# Introduction

A simple example/skeleton of modular sinatra app.

The choosen web server for this dev-env is 'puma'.

# Requirements

You need nothing but, of course, 'git' and 'docker'.

[Check instructions](https://docs.docker.com/installation) for installing 'docker' in your platform.

# Usage

## Using 'baids'

[baids (bash AIDs)](https://github.com/rcmorano/baids) is just a bash functions and aliases manager. 

This repo contains a set of functions that may help you. You choose :]

```
APP_NAME='example-app'
git clone https://rcmorano@bitbucket.org/rcmorano/sinatra-bootstrap-docker-dev-env.git
ln -s $PWD/sinatra-bootstrap-docker-dev-env/extras/baids/functions.d ~/.baids/functions.d/sinatra-bootstrap-docker-dev-env
baids-reload
sinatra-bootstrap-dde-build $APP_NAME # argument must be a dir under the current path or an existing fullpath
sinatra-bootstrap-dde-run $APP_NAME # argument must be...
# sinatra-bootstrap-dde-run-autocommit $APP_NAME
```

You might want to remove/change/add some custom 'functions' for your project development.

## Raw usage

```
APP_NAME='example-app'
git clone https://rcmorano@bitbucket.org/rcmorano/sinatra-bootstrap-docker-dev-env.git $APP_NAME
docker build -t $USER/${APP_NAME}-dev-env - < $APP_NAME/Dockerfile
docker run -t -i -p 9292 --name $APP_NAME -v $PWD/$APP_NAME:/$APP_NAME ${APP_NAME}-dev-env
```

With any of these methods, you'll get an interactive shell instance into a 'docker' container ready to run your cloned sinatra app inside of it.

Now just jump into your cloned git repository, which is bound inside the container at '/$APP_NAME' dir and launch your app:

```
root@bc385cac376d:/example-app# rackup
Puma 2.8.2 starting...
* Min threads: 0, max threads: 16
* Environment: development
* Listening on tcp://0.0.0.0:9292
```

And as last but not least, get the URI where your app is listening and access from your favorite browser:

```
echo http://localhost:$(docker port $APP_NAME 9292| sed 's|\(.*\):\(.*\)|\2|g')
```
