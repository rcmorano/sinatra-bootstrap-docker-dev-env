#$:.unshift(File.expand_path('../../lib', __FILE__))
 
require 'sinatra/base'
require 'sinatra/reloader' if ENV['RACK_ENV'] == 'development'
 
class AppController < Sinatra::Base

  configure :development do
    register Sinatra::Reloader
  end 

  set :views, File.expand_path('../../views', __FILE__)
  set :public_dir, 'public'
  enable :sessions
 
end
